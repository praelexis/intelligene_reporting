# Software Copyright Notice
# Copyright 2021 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    Capital Place F,
#    15-21 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

import pandas as pd

class GeneticRiskScorer:
    """
    Score the genetic results, determine the risk of each health category and identify the impact based on the risk.
    """
    def __init__(self, gene_logic_df, category_df, apoe_compare_df, apoe_score_df, exceptions_df, impact_df):
        """
        :param gene_logic_df: General gene scoring rules.
        :param category_df: Health categories populated with the corresponding alleles.
        :param apoe_compare_df: Comparison of the rs429358 and rs7412 alleles for the APOE gene.
        :param apoe_score_df: Gene scores for the APOE gene based on the rs429358 and rs7412 comparison.
        :param exceptions_df: Gene scoring exceptions rules based on the health category it is in.
        :param impact_df: Impact descriptions based on the risk score for each health category.
        """
        self.gene_score_df = gene_logic_df
        self.category_df = category_df
        self.apoe_compare_df = apoe_compare_df
        self.apoe_score_df = apoe_score_df
        self.exceptions_df = exceptions_df
        self.impact_df = impact_df
        self.client_data_df = None

    def determine_apoe_intermediate(self):
        """
        Create an intermediate gene result for the APOE gene based on the rs429358 and rs7412 alleles.

        :param client_data_df: Tested genes with their respective result
        :returns: apoe_intermediate_df: APOE gene with its respective intermediate result.
        """

        # Alleles of interest
        allele_1 = 'rs429358'
        allele_2 = 'rs7412'

        # Extract data for alleles from client data
        allele_1_call = self.client_data_df.loc[self.client_data_df['SNP'] == allele_1, 'Call'].iloc[0]
        allele_2_call = self.client_data_df.loc[self.client_data_df['SNP'] == allele_2, 'Call'].iloc[0]

        # Create an allele DataFrame
        apoe_allele_df = pd.DataFrame(
            {allele_1: [allele_1_call], allele_2: [allele_2_call], 'SNP': allele_1+','+allele_2})

        # Identify and add intermediate result
        apoe_inter_df = apoe_allele_df.merge(self.apoe_compare_df,
                                             left_on=[allele_1, allele_2],
                                             right_on=[allele_1, allele_2])[['SNP', 'Result']]

        return apoe_inter_df

    def group_data_into_categories(self):
        """
        Group the different alleles in the data based on the corresponding health categories and corrects entries for
        APOE genes.

        :param lient_data_df: Tested genes with their respective result.
        :returns grouped_df: Tested alleles in thier respective health category with their corresponding
        result.
        """

        # Group client data into corresponding health categories
        category_alleles_df = self.category_df.merge(self.client_data_df,
                                                     left_on=['SNP'],
                                                     right_on=['SNP'],
                                                     how='left')[['Categories', 'Gene Symbol', 'SNP', 'Call']]

        # Create a DataFrame with the intermediate APOE gene result
        apoe_inter_df = self.determine_apoe_intermediate()

        # Add a column with APOE results
        grouped_df = category_alleles_df.merge(apoe_inter_df,
                                               left_on=['SNP'],
                                               right_on=['SNP'],
                                               how='left')[['Categories', 'Gene Symbol', 'SNP', 'Call', 'Result']]

        # Combine APOE 'Result' column into 'Call' column and drop APOE 'Result' column
        grouped_df['Call'] = grouped_df['Call'].combine_first(grouped_df['Result'])
        grouped_df.drop(labels=['Result'], axis=1, inplace=True)

        return grouped_df

    def score_genes(self, client_data_df):
        """
        Score the different genes in the data based on allele result and health category

        :param client_data_df: Tested genes with their respective result.
        :returns gene_scored_df: Scored genes grouped in the corresponding health categories.
        """
        self.client_data_df = client_data_df
        processed_df = self.group_data_into_categories()

        # Score genes based on general logic by adding a 'Gene Score' column
        gene_scored_df = processed_df.merge(self.gene_score_df,
                                            left_on=['SNP', 'Call'],
                                            right_on=['dbSNP', 'value'],
                                            how='left')[['Categories', 'Gene Symbol', 'SNP', 'Call', 'variable']]

        gene_scored_df.rename({'variable': 'Gene Score'}, axis=1, inplace=True)

        # Add scores for APOE genes
        gene_scored_df = gene_scored_df.merge(self.apoe_score_df,
                                              left_on=['Categories', 'Call'],
                                              right_on=['variable', 'Result'],
                                              how='left')\
                                              [['Categories', 'Gene Symbol', 'SNP', 'Call', 'Gene Score', 'value']]

        gene_scored_df['Gene Score'] = gene_scored_df['Gene Score'].combine_first(gene_scored_df['value'])
        gene_scored_df.drop(labels=['value'], axis=1, inplace=True)
        gene_scored_df.dropna(subset=['Call'], inplace=True)

        # Correct for exceptions
        gene_scored_df = gene_scored_df.merge(self.exceptions_df,
                                              left_on=['Categories', 'Call', 'SNP'],
                                              right_on=['Category', 'value', 'SNP'],
                                              how='left')\
                                              [['Categories', 'Gene Symbol', 'SNP', 'Call', 'Gene Score', 'variable']]

        gene_scored_df['variable'] = gene_scored_df['variable'].combine_first(gene_scored_df['Gene Score'])
        gene_scored_df.drop(labels=['Gene Score'], axis=1, inplace=True)
        gene_scored_df.rename({'variable': 'Gene Score'}, axis=1, inplace=True)
        gene_scored_df['Gene Score'] = gene_scored_df['Gene Score'].astype(float).astype(int)

        return gene_scored_df

    def scale_risk(self, client_data_df):
        """
        Scores the genes and determine the genetic risk of each health category.

        :param client_data_df: Tested genes with their respective result.
        :returns risk_df: DataFrame containing all the health categories and their corresponding risk (1-5)
        """
        gene_scores_df = self.score_genes(client_data_df)

        def counter(df):
            max_val = len(df) * 2
            count = sum(df)
            risk = round(count / (max_val / 5))
            if risk == 0:
                return 1
            return risk


        risk_df = pd.DataFrame(gene_scores_df.groupby('Categories')['Gene Score'].apply(counter))
        risk_df.rename(columns={'Gene Score': 'Genetic Risk'}, inplace=True)

        return risk_df

    def determine_impact(self, client_data_df):
        """
        Determine the impact based on the scaled risk score.

        :param client_data_df: Tested genes with their respective result.
        :returns impact_scored_df: Impacts for every health category based on the risk score.
        """
        risk_df = self.scale_risk(client_data_df)
        risk_df = risk_df.reset_index()

        risk_df['Categories'] = risk_df['Categories'].str.lower()
        self.impact_df['Affected'] = self.impact_df['Affected'].str.lower()

        impact_scored_df = risk_df.merge(self.impact_df,
                                  left_on=['Categories', 'Genetic Risk'],
                                  right_on=['Affected', 'variable'],
                                  how='left')[['Categories','Genetic Risk', 'value']]

        return impact_scored_df

