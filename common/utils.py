# Software Copyright Notice
# Copyright 2021 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    Capital Place F,
#    15-21 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

import json
import pandas as pd


def write_json(record, filename):
    with open(filename, 'w') as f:
        json.dump(record, f)


def read_json(filename):
    with open(filename) as f:
        return json.load(f)


def append_two_dicts(dict_a, dict_b):
    return dict(list(dict_a.items()) + list(dict_b.items()))


def create_and_save_html_dict(df, key, filepath):
    """
    Save dataframe in a dictionary, with a key that is used in the HTML template, and writer it to a json file.
    """
    template_vars = {key: df.to_html(index=False)}
    write_json(template_vars, filepath)
    print(f'Created {filepath}')


def create_and_save_styled_html_dict(stylerdf, key, filepath, class_name):
    """
    Save STYLED dataframe in a dictionary, with a key that is used in the HTML template, and write it to a json file.
    """
    html = stylerdf.hide_index().set_table_attributes(f"class={class_name}").render()
    template_vars = {key: html}
    write_json(template_vars, filepath)
    print(f'Created {filepath}')


def append_dicts_in_dir(path_to_dir, merged_json_file_path):
    """
    Combine json files in a directory into a single json file.

    :param path_to_dir: string path to directory where json files are located
    :param merged_json_file_path: string path to merged json file
    """
    from os import listdir
    from os.path import isfile, join

    onlyfiles = [f for f in listdir(path_to_dir) if isfile(join(path_to_dir, f))]

    result = {}

    for file in onlyfiles:
        json_1 = read_json(path_to_dir + file)
        result = dict(list(result.items()) + list(json_1.items()))


    write_json(result, merged_json_file_path)


def create_rendered_html(html_template_path, html_template, rendered_template_name, path_to_json):
    """
    Add dataframes and variables from the merged json file to the HTML template.
    """
    from jinja2 import Environment, FileSystemLoader

    env = Environment(loader=FileSystemLoader(html_template_path))

    # Read in the template of the final_html format (containing the html_variable_name s)
    template = env.get_template(html_template)

    template_vars = read_json(path_to_json)
    # Read in and append the dict in example_tables directory
    html_out = template.render(template_vars)

    with open(rendered_template_name, 'w') as f:
        f.write(html_out)


def data_transform(client_data_df, drop_labels):
    """
    Remove null and unwanted columns and rows. Remove unwanted characters. Standardise column headings.

    :param client_data_df: DataFrame containing client data.
    :param drop_labels: A list of labels that has to be dropped.
    :returns client_data_df: DataFrame containing client data without null values and unwanted characters.
    """
    trans_df = client_data_df.copy()

    # Drop all null rows and columns
    trans_df.dropna(how='all', inplace=True)
    trans_df.dropna(axis=1, how='all', inplace=True)

    # Reset the index
    trans_df.reset_index(drop=True, inplace=True)

    # Drop unwanted labels
    trans_df.drop(labels=drop_labels, axis=1, inplace=True)

    # Remove '/' from data
    trans_df['Call'] = trans_df['Call'].str.replace(r'/', '')

    # Standardise column names
    trans_df.rename(columns={'NCBI SNP Reference': 'SNP'}, inplace=True)

    return trans_df


def split_df(large_df, col_name):
    """
    Split the dataframe based on groupings of identical entries in the given column and create a heading for each group.

    :param large_df: DataFrame that needs to be split.
    :param col_name: Column name that contains the entries that determines the groupings of the large_df.
    :return df_dict: Dictionary containing the heading as the key corresponding to the specific DataFrame.
    """
    categories = pd.Series(large_df[col_name].unique())
    grouped_df = large_df.groupby(large_df[col_name])
    df_dict = {}

    for heading in categories:
        temp_df = grouped_df.get_group(heading).copy()
        temp_df.drop(columns=[col_name], inplace=True)
        temp_df.columns = pd.MultiIndex.from_product([[heading], temp_df.columns])
        df_dict[heading] = temp_df

    return df_dict


def profile_page_format(df):
    df_copy = df.copy()
    # Set default color and font
    df_copy.loc[:,:] = 'color: #2A2A5A; font-size: 11pt'
    # Set particular cell colors
    df_copy.iloc[0,0] = 'color: #2A2A5A; font-size: 14pt'
    df_copy.iloc[1,0] = 'color: red; font-size: 16pt'
    return df

def add_complimentary_gene_rules(df, snp_col_name):
    """
    Create and add the gene scoring rules that incorporates the possible complimentary result.

    :param df: Gene scoring rules dataframe.
    :param snp_col_name: Name of the column that contains the SNP names.
    :returns new_gene_rules_df: Concatenated dataframe contained all possible genetic scoring rules for the given df.
    """

    # Create a dataframe containing the complimentary gene scoring rules
    to_replace_dict = {'C': 'G', 'G': 'C', 'A': 'T', 'T': 'A'}
    reverse_df = df.copy()
    reverse_df['value'].replace(to_replace_dict, regex=True, inplace=True)

    # Remove entries that would not change because of the complimentary gene scoring rules
    snp_list = pd.Series(reverse_df[snp_col_name].unique())
    for snp in snp_list:
        reverse_slice = reverse_df.query(f"{snp_col_name} == @snp")
        original_slice = df.query(f"{snp_col_name} == @snp")

        reverse_slice.reset_index(inplace=True)
        original_slice.reset_index(inplace=True)

        if reverse_slice.loc[0, 'value'] == original_slice.loc[3, 'value']:
            index_names = reverse_df[reverse_df['dbSNP'] == snp].index
            reverse_df.drop(index_names, inplace=True)

    # Add the complimentary gene scoring rules to the original df
    new_gene_rules_df = pd.concat([df, reverse_df], ignore_index=True)

    return new_gene_rules_df
