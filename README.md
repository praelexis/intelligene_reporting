1. Reading in raw data CSV files
2. ETL step 
    - Remove the metadata
    - Remove NULL rows and columns
    - String manipulation
3. Reading and preprocessing scoring logic from excel sheets
    - Process and create new excel shhets that contain independent scoring logic
    - Read in the scoring logic
4. For one client generate gene scored data
    - Merge the gene scores to the to each of the client's data
5. For one client calculate risk factor and determine genetic impact
    - Group genes together based on health category using the risk scoring logic. 
    - Calculate each health category score (the sum of the grouped gene scores (out of 2))
    - Take note of upper limit health condition score and scale the score to a risk factor between 1 and 5.
    - Apply lookup table to risk factors to get the 'genetic impact' of each health condition
6. Create HTML template
    - Create the metadata in HTML template.
    - Insert genetic impact variable name in correct location in the HTML template.
    - Create json file key mapping with variable name.
    - Use jinja to insert the variables into the rendered HTML file.
7. Create CSS styling for report
    - Insert front page 
    - Insert footer.
    - Insert company colors
8. Generate PDF from HTML template
    - Use weazyprint to generate PDF file from rendered HTML file.